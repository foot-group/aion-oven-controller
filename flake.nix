{
  description = "NDSP for the aion oven";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  inputs.sipyco.url = github:m-labs/sipyco;  
  inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, sipyco }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
    in rec {

      packages.x86_64-linux.pymodbustcp = pkgs.python3Packages.buildPythonPackage rec {
        pname = "pymodbustcp";
        version = "0.1.10";
        src = pkgs.python3Packages.fetchPypi {
          pname = "pyModbusTCP";
          inherit version;
          sha256 = "88079e718d0d43a4a26c1792cce1b6bf058f533050fef40185f9e521db7666cd";
        };
#        propagatedBuildInputs = with pkgs.python3Packages; [  ];
      };

      packages.x86_64-linux = {
        aion-oven-controller = pkgs.python3Packages.buildPythonPackage {
          pname = "aion_oven_controller";
          version = "0.1";
          src = self;
          propagatedBuildInputs = [ sipyco.packages.x86_64-linux.sipyco packages.x86_64-linux.pymodbustcp ];
        };
      };

      defaultPackage.x86_64-linux = pkgs.python3.withPackages(ps: [ packages.x86_64-linux.aion-oven-controller ]);

      devShell.x86_64-linux = pkgs.mkShell {
        name = "aion-oven-controller-dev-shell";
        buildInputs = [
          (pkgs.python3.withPackages(ps: [ sipyco.packages.x86_64-linux.sipyco packages.x86_64-linux.aion-oven-controller ]))
        ];
      };
    };
}
