#!/usr/bin/env python3

import sys
from setuptools import setup
from setuptools import find_packages

setup(
   name="aion_oven_controller",
   version="0.1",
   description="AION Oven controller",
   author="Elliot Bentine",
   author_email="elliot.bentine@physics.ox.ac.uk",
   url="https://gitlab.com/foot-group/aion-oven-controller",
   download_url="https://gitlab.com/foot-group/aion-oven-controller",
   packages=find_packages(),
   install_requires=["pyModbusTCP==0.1.10"],
   entry_points={
      "console_scripts": [
         "run_oven_controller = aion_oven_controller.run:main",
      ]
   },
   license="LGPLv3+",
)
