from pyModbusTCP.client import ModbusClient

class Device:
	
	
	def __init__(self, host,timeout=30):
		self.host = host
		self.is_open = False
		self.timeout = timeout
		self.comm = ModbusClient(host=self.host,timeout=self.timeout)
		self.open()
		self.need_pv_decimals = True
		self.need_input_decimals = True
		self.get_decimals()
	
	
	def ping(self):
		return True
	
	
	def get_decimals(self):
		counter = 0
		while self.need_pv_decimals or self.need_input_decimals: # Attempt to catch timeouts
			if self.need_pv_decimals:
				try:
					# register 1922 corresponds to decimal point position of PV (found using iTools)
					self.pv_decimals = self.comm.read_input_registers(1922)[0]
					self.need_pv_decimals = False
				except TypeError:
					if counter>=8:
						print("Failed to retrieve value from register 1922 in 8 attempts")
						break
			if self.need_input_decimals:
				try:
					# register 2309 corresponds to decimal value place of remote input (found using iTools)
					self.input_decimals = self.comm.read_input_registers(2309)[0]
					self.need_input_decimals = False
				except TypeError:
					if counter>=8:
						print("Failed to retrieve value from register 2309 in 8 attempts")
						break
			counter+=1
	
	
	def open(self):
		if not self.comm.is_open():
			self.is_open = self.comm.open()
		else:
			self.is_open = self.comm.is_open()
	
	
	def close(self):
		if self.comm.is_open():
			self.is_open = not self.comm.close() # self.comm.close returns True if connection successfully closes
		else:
			self.is_open = self.comm.is_open()
	
	
	def get_temperature(self,close=True):
		self.open()
		
		try:
			# register 1 corresponds to process variable (analog input 1), i.e. current temperature, use iTools to find others.
			result = self.comm.read_input_registers(1)[0]
			temperature = result / 10**self.pv_decimals
		except TypeError:
			self.is_open = False # timeout closes socket
			temperature = None
		
		if close:
			self.close()
		
		return temperature
	
	
	def set_temperature(self,setpoint,close=True):
		"""setpoint in degrees Celsius"""
		self.open()
		
		# register 2 corresponds to setpoint before rate limiting (found using iTools)
		success = self.comm.write_single_register(2,int(setpoint*10**self.input_decimals))
		
		if close:
			self.close()
		
		# success == True if write is successful
		return success
	
	
	def set_setpoint_up_rate_limit(self,rate,close=True):
		"""limit of increase of working setpoint in degrees Celsius per second"""
		self.open()
		
		# register 35 corresponds to setpoint up rate limit (found using iTools)
		success = self.comm.write_single_register(35,int(rate*10**self.input_decimals))
		
		if close:
			self.close()
		
		# success == True if write is successful
		return success
