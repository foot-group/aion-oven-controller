import argparse
import logging
import sys
import asyncio

from aion_oven_controller.controller import Device

from sipyco.pc_rpc import Server
from sipyco import common_args

def get_argparser():
   parser = argparse.ArgumentParser(
      description="""Controller for the aion Sr oven.""")
   parser.add_argument("-d", "--device", default=None, required=True, help="Device host name or ip address.")
   parser.add_argument("-t", "--timeout", default=30, help="Maximum wait time in seconds for communication via TCP")
   common_args.simple_network_args(parser, 3272)
   common_args.verbosity_args(parser)
   return parser

def main():
   args = get_argparser().parse_args()
   common_args.init_logger_from_args(args)

   async def run():
      server = Server({"device": Device(args.device,timeout=args.timeout)}, None, True)
      await server.start(common_args.bind_address_from_args(args), args.port)
      try:
         await server.wait_terminate()
      finally:
         await server.stop()

   loop = asyncio.get_event_loop()
   try:
      loop.run_until_complete(run())
   except KeyboardInterrupt:
      pass
   finally:
      loop.close()

   if __name__ == "__main__":
      main()
