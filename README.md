# Oven controller

A Network Device Support Package (NDSP) to interface the Eurotherm EPC3000 series of oven controllers. The EPC3016 is used to control the strontium oven in the AION sidearms.

## Configuring and using the EPC3000

You have to navigate a fairly nasty and confusing set of menus to configure the device. You definitely want to check the manual.
* The first time you start the controller it will request quick start codes which help set the device up with sensible default parameters. For the `SET 1` parameters, use `1.K.5.X.X` and for `SET 2` use `X.X.X.X.C`. You can re-enter this quick start configuraton by holding the page key while restarting the device.
* The controller defaults to manual mode when you turn it on (indicated by a little hand on the device). To enable the PID loop you need to switch it to 'auto' mode, which you can do by holding the up and down keys to bring up a menu.
* You can enter 'configuration mode' by holding the page button until 'Lev 3' shows. Then press up to show 'Conf', and press return. Enter the passcode (default 0004).
* Once in configuration mode, you can navigate menus using the page key to cycle the main lists, and return and up/down keys to explore the sub lists.
* In `Loop / SP` you can find a `SP.UP` property, which can be used to set a limit on how quickly the set point ramps up. It looks like the units are degrees/s, but the manual isn't explicit. I've set ours to `0.1`, the lowest setting. 
* Under `Comm / nwrk` the `IP.MD` property can be used to configure the controller to use DHCP.

## Resources

The manual for the EPC3000 series can be found [here](https://processcontrol.tv/manuals/EPC3000%20User%20Manual.pdf).

